var global = require('./settings');
var dateFormat = require('dateformat');
var config = require('./libs/config');
var log = require('./libs/log')(module);
var JDBC = require('jdbc');
var fs = require('fs');
var jinst = require('jdbc/lib/jinst');
//var asyncjs         = require('async');
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var dbFolder = '/opt/tochki/';
var options = ['DENERGY', 'MENERGY', 'HENERGY', 'WORK_DATA'];
var dbConfig = {
    // url: 'jdbc:derby:/opt/tochki/' + dataset.location,
    minpoolsize: 10,
    maxpoolsize: 100,
    user: '',
    password: '',
    properties: {}
};


server.listen(global.config.port, function () {
    console.log('listening on *:' + global.config.port);
});

app.use(express.static(__dirname + '/public'));

if (!jinst.isJvmCreated()) {
    jinst.addOption("-Xrs");
    jinst.setupClasspath([__dirname + '/drivers/derbydb.jar',
        __dirname + '/drivers/derby.jar',
        __dirname + '/drivers/derbyclient.jar',
        __dirname + '/drivers/derbytools.jar']);
}
io.on('connection', function (socket) {
    // console.log(config.get('reports:timeReport'));

    console.log('a user connected');
    socket.on('time_report', function (dataset) {
        dataset.socket = socket;
        timeReport(dataset);
    });
    socket.on('PVINPUTVOLTAGE', function (dataset) {
        console.log(dataset);
        dataset.socket = socket;
        dataset.dataLabel = 'listPVINPUTVOLTAGE';
        // dataset.SQL = "SELECT CURRENTTIME, PVINPUTPOWER1, PVINPUTPOWER2 FROM WORK_DATA WHERE SERIALNO='" + dataset.invertor + "'";
        dataset.SQL = "SELECT CURRENTTIME, PVINPUTVOLTAGE1, PVINPUTVOLTAGE2 FROM WORK_DATA WHERE SERIALNO='" + dataset.invertor + "'";
        if (dataset.datestart !== "") {
            dataset.SQL += " AND CURRENTTIME >= '" + dataset.datestart + " 00:00:00.0'";
        }
        if (dataset.dateend !== "") {
            dataset.SQL += " AND CURRENTTIME <= '" + dataset.dateend + " 23:59:59.9'";
        }
        dataset.SQL += " ORDER BY CURRENTTIME";
        getData(dataset);
    });
    socket.on('PVINPUTPOWER', function (dataset) {
        console.log(dataset);
        dataset.socket = socket;
        dataset.dataLabel = 'listPVINPUTPOWER';
        // dataset.SQL = "SELECT CURRENTTIME, PVINPUTPOWER1, PVINPUTPOWER2 FROM WORK_DATA WHERE SERIALNO='" + dataset.invertor + "'";
        dataset.SQL = "SELECT CURRENTTIME, PVINPUTPOWER1, PVINPUTPOWER2 FROM WORK_DATA WHERE SERIALNO='" + dataset.invertor + "'";
        if (dataset.datestart !== "") {
            dataset.SQL += " AND CURRENTTIME >= '" + dataset.datestart + " 00:00:00.0'";
        }
        if (dataset.dateend !== "") {
            dataset.SQL += " AND CURRENTTIME <= '" + dataset.dateend + " 23:59:59.9'";
        }
        dataset.SQL += " ORDER BY CURRENTTIME";
        getData(dataset);
    });

    socket.on('massivereport', function (dataset) {
        dataset.socket = socket;
        console.log('massive report');
        massivereport(dataset);
    });
    socket.on('getListAvailabelOptions', function (dataset) {
        optionsList(dataset, socket);
        console.log(JSON.stringify(dataset));
    });
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
    socket.on('getLocationsList', function () {
        console.log('getLocationsList');
        locationsList(dbFolder, socket);
    });
    socket.on('getInvertorsList', function (dataset) {
        console.log('getInvertorsList');
        invertorsList(dataset, socket);
    });
    socket.on('getData', function (dataset) {
        getDataForGraph(dataset, socket);
        //socket.emit('data', getData(dataset));
    });
});

// var getData = function (dataset) {
//     return dataset;
// };
var massivereport = function (dataset) {
    console.log('massive Report');
    console.log(dataset);
    dataset.dataLabel = 'massivereport';
    // dataset.SQL = "SELECT CURRENTTIME, PVINPUTPOWER1, PVINPUTPOWER2 FROM WORK_DATA WHERE SERIALNO='" + dataset.invertor + "'";
    dataset.SQL = "SELECT CURRENTTIME, PVINPUTPOWER1, PVINPUTPOWER2 FROM WORK_DATA WHERE SERIALNO='" + dataset.invertor + "'";
    if (dataset.datestart !== "") {
        dataset.SQL += " AND CURRENTTIME >= '" + dataset.datestart + " 00:00:00.0'";
    }
    if (dataset.dateend !== "") {
        dataset.SQL += " AND CURRENTTIME <= '" + dataset.dateend + " 23:59:59.9'";
    }
    dataset.SQL += " ORDER BY CURRENTTIME";
    getData(dataset);
};
var timeReport = function (dataset) {

    // console.log(dataset.label);
    dataset.dataLabel = "timeReport";
    dataset.SQL = "SELECT GRIDVOLTAGER, TOTALACOUTPUTACTIVEPOWER, CURRENTTIME FROM WORK_DATA WHERE SERIALNO='" + dataset.invertor + "'";
    if (dataset.datestart !== "") {
        dataset.SQL += " AND CURRENTTIME >= '" + dataset.datestart + " 00:00:00.0'";
    }
    if (dataset.dateend !== "") {
        dataset.SQL += " AND CURRENTTIME <= '" + dataset.dateend + " 23:59:59.9'";
    }
    dataset.SQL += " ORDER BY CURRENTTIME";
    getData(dataset);
};

var getDataForGraph = function (data, socket) {


    console.log(data.element.toLowerCase());

    console.log(data);
    data.socket = socket;
    var report = config.get('reports:' + data.element);
    data.dataLabel = report.name;
    console.log(report);
    data.SQL = report.SQL + ' WHERE SERIALNO = \'' + data.invertor + '\'';
    if (data.datestart !== "") {
        var start = new Date(data.datestart);
        console.log(dateFormat(start, report.timeFormat));
        data.SQL += " AND " + report.timeName + " >= '" + dateFormat(start, report.timeFormat) + "'";
    }
    if (data.dateend !== "") {
        var end = new Date(data.dateend);
        console.log(end);
        data.SQL += " AND " + report.timeName + " <= '" + dateFormat(end, report.timeFormat).replace('00:00:00.0', '23:59:59.9') + "'";
    }
    data.SQL += " ORDER BY " + report.order;
    console.log(data.SQL);
    getData(data);
    // console.log(data);
}

// var getDataForGraph = function (data, socket) {
//     console.log('getData');
//     console.log(JSON.stringify(data));
//     var dataset = [];
//     var timeName = "";
//     dataset.dataLabel = "list" + data.element;
//     dataset.socket = socket;
//     dataset.location = data.location;
//
//     if (data.element == "MENERGY") {
//         timeName = "YEARMONTH";
//     }
//     else if (data.element == "WORK_DATA") {
//         timeName = "CURRENTTIME";
//     }
//     else {
//         timeName = "TRANDATE";
//     }
//     if (data.element == "WORK_DATA") {
//         // dataset.SQL = "SELECT * FROM (SELECT ROW_NUMBER() OVER() AS rownum, WORK_DATA.* FROM WORK_DATA) AS tmp WHERE rownum = 1"
//         // CURRENTTIME, PVINPUTPOWER1, PVINPUTPOWER2, PVINPUTVOLTAGE1, PVINPUTVOLTAGE2
//         dataset.SQL = "SELECT * FROM WORK_DATA WHERE SERIALNO = '"
//             + data.invertor + "'";
//         if (data.datestart !== "") {
//             dataset.SQL += " AND CURRENTTIME >= '" + data.datestart + " 00:00:00.0'";
//         }
//         if (data.dateend !== "") {
//             dataset.SQL += " AND CURRENTTIME <= '" + data.dateend + " 23:59:59.9'";
//         }
//         dataset.SQL += " ORDER BY CURRENTTIME";
//     }
//     else {
//         dataset.SQL = "SELECT * FROM " + data.element + " WHERE SERIALNO = '" + data.invertor + "'";
//         if (data.element === "MENERGY") {
//
//         }
//         else {
//             if (data.datestart !== "") {
//                 dataset.SQL += " AND " + timeName + " >= '" + data.datestart + " 00:00:00.0'";
//             }
//             if (data.dateend !== "") {
//                 dataset.SQL += " AND " + timeName + " <= '" + data.dateend + " 23:59:59.9'";
//             }
//         }
//
//     }
//     dataset.SQL += " ORDER BY " + timeName + "";
//     console.log(dataset.SQL);
//     getData(dataset);
// };
//

var invertorsList = function (location, socket) {
    var dataset = [];
    dataset.dataLabel = "invertorsList";
    let banned = config.get('banned');
    let bannedSTR = "";
    for (let i in banned){
        bannedSTR += "'" + banned[i] + "',"
    }
    bannedSTR = bannedSTR.replace(/,$/,"");
    dataset.SQL = "SELECT * FROM SERIALNO WHERE SERIALNO NOT IN (" + bannedSTR + ")";
    console.log(dataset.SQL);
    dataset.socket = socket;
    dataset.location = location.element;
    console.log("location:" + location.element);
    getData(dataset);
};

var locationsList = function (dir, socket) {
    fs.readdir(dir, function (err, items) {
        console.log(items);
        socket.emit('locationsList', items);
    });
};
var optionsList = function (data, socket) {
    socket.emit('listAvailabelOptions', config.get('invertors:' + data.element + ':reports'));
};


var getData = function (dataset) {
    // console.log(dataset);
    console.log((dataset.location));
    dbConfig.url = 'jdbc:derby:' + dbFolder + dataset.location;
    var derbydb = new JDBC(dbConfig);

    derbydb.initialize(function (err) {
        if (err) {
            console.log(err);
        }
        derbydb.reserve(function (err, connObj) {
            if (connObj) {
                select(connObj, dataset);
            }
        });
    });
};

var select = function (connObj, dataset) {
    console.log('start SQL');
    var conn = connObj.conn;
    conn.createStatement(function (err, statement) {
        if (err) {
            console.log(err);
        } else {
            statement.setFetchSize(100, function (err) {
                if (err) {
                    console.log(err);
                } else {
                    statement.executeQuery(dataset.SQL,
                        function (err, resultset) {
                            if (err) {
                                console.log(err)
                            } else {
                                resultset.toObjArray(function (err, results) {
                                    console.log(dataset.dataLabel);
                                    dataset.socket.emit(dataset.dataLabel, results);

                                    // console.log(JSON.stringify(results));
                                });
                            }
                        });
                }
            });
        }
    });
};
