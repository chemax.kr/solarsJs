/**
 * Created by chemax on 08.08.2017.
 */
var mongoose    = require('mongoose');
var log         = require('./log')(module);
var config      = require('./config');

mongoose.connect(config.get('mongoose:uri'));
var db = mongoose.connection;

db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback () {
    log.info("Connected to DB!");
});

var Schema = mongoose.Schema;


var Settings = new Schema({
    name: { type: String, required: true },
    value: { type: String, required: true },
    description: { type: String, required: true },
    modified: { type: Date, default: Date.now }
});

// validation
Settings.path('name').validate(function (v) {
    return v.length > 3 && v.length < 70;
});

var SettingsModel = mongoose.model('Settings', Settings);

module.exports.SettingsModel = SettingsModel;