/**
 * Created by chemax on 18.07.2017.
 */
var socket = io.connect('http://192.168.22.70:3000');
var invertor = 0;

socket.on('connect', function () {
    console.log('connection establish');
    socket.emit("getLocationsList");
});

socket.on('listAvailabelOptions', function (data) {
    // optionsList.elements = data;
    console.log(data);
    // var arr = Object.keys(data[0]);
    for (i in data) {
        optionsList.elements.push({id: data[i], text: localize(data[i])});
    }

    // if (data[0][arr[0]] > 0) {
    //     optionsList.elements.push({id: arr[0], text: localize(arr[0])});
    // }
});

socket.on('MENERGY', function (data) {
    //console.log(data);
    var menergy = {
        labels: [], datasets: [{label: localize("MENERGY"), data: []}],
        headerName: "[" + localize(invertorsList.location) + "]" + localize(invertor) + ": " + localize("MENERGY")
    };
    var labels = [];
    var dataG = [];
    for (i in data) {
        labels.push(data[i].YEARMONTH);
        dataG.push(data[i].ENERGY);
    }
    menergy.labels = labels;
    menergy.datasets[0].data = dataG;
    vm2.graphicsdata.push(menergy);
});

socket.on('massiveReportM', function (data) {
    console.log(data);
    var arr = [];
    var arr2 = [];
    for (d in data) {
        var key = data[d].CURRENTTIME.replace(/-\d\d \d\d:\d\d:.+/, "");
        if (typeof arr[key] === "undefined") {
            arr[key] = [];
        }
        if (typeof arr2[key] === "undefined") {
            arr2[key] = [];
        }
        arr[key].push(parseInt(data[d].PVINPUTPOWER1));
        arr2[key].push(parseInt(data[d].PVINPUTPOWER2));
    }
    var pv1 = [];
    var pv2 = [];
    var labels = [];
    for (x in arr) {
        var resultGenerator = arr[x].reduce(function (sum, current) {
            return sum + current;
        }, 0);
        var resultGenerator2 = arr2[x].reduce(function (sum, current) {
            return sum + current;
        }, 0);


        labels.push(x);
        pv1.push((resultGenerator / arr[x].length).toFixed(1));
        pv2.push((resultGenerator2 / arr2[x].length).toFixed(1));
        resultGenerator = 0;

    }
    var pv = {
        labels: [], datasets: [],
        headerName: "[" + localize(invertorsList.location) + "]" + localize(invertor) + ": " + localize("massiveReportM")
    };
    pv.labels = labels;
    pv.datasets.push({
        label: localize(invertor + 'PVINPUTPOWER1'),
        data: pv1,
        type: 'bar',
        backgroundColor: "rgba(41, 125, 44, 0.5)"
    });
    pv.datasets.push({
        label: localize(invertor + 'PVINPUTPOWER2'),
        data: pv2,
        type: 'bar',
        backgroundColor: "rgba(41, 0, 44, 0.5)"
    });
    vm2.graphicsdata.push(pv);
});

socket.on('massiveReportH', function (data) {
    console.log(data);
    var arr = [];
    var arr2 = [];
    for (d in data) {
        var key = data[d].CURRENTTIME.replace(/ \d\d:\d\d:.+/, "");
        if (typeof arr[key] === "undefined") {
            arr[key] = [];
        }
        if (typeof arr2[key] === "undefined") {
            arr2[key] = [];
        }
        arr[key].push(parseInt(data[d].PVINPUTPOWER1));
        arr2[key].push(parseInt(data[d].PVINPUTPOWER2));
    }
    var pv1 = [];
    var pv2 = [];
    var labels = [];
    for (x in arr) {
        var resultGenerator = arr[x].reduce(function (sum, current) {
            return sum + current;
        }, 0);
        var resultGenerator2 = arr2[x].reduce(function (sum, current) {
            return sum + current;
        }, 0);


        labels.push(x);
        pv1.push((resultGenerator / arr[x].length).toFixed(1));
        pv2.push((resultGenerator2 / arr2[x].length).toFixed(1));
        resultGenerator = 0;

    }
    var pv = {
        labels: [], datasets: [],
        headerName: "[" + localize(invertorsList.location) + "]" + localize(invertor) + ": " + localize("massiveReportH")
    };
    pv.labels = labels;
    pv.datasets.push({
        label: localize(invertor + 'PVINPUTPOWER1'),
        data: pv1,
        type: 'bar',
        backgroundColor: "rgba(41, 125, 44, 0.5)"
    });
    pv.datasets.push({
        label: localize(invertor + 'PVINPUTPOWER2'),
        data: pv2,
        type: 'bar',
        backgroundColor: "rgba(41, 0, 44, 0.5)"
    });
    vm2.graphicsdata.push(pv);
});

socket.on('massiveReport', function (data) {
    console.log(data);
    var arr = [];
    var arr2 = [];
    for (d in data) {
        var key = data[d].CURRENTTIME.replace(/:\d\d:.+/, "");
        if (typeof arr[key] === "undefined") {
            arr[key] = [];
        }
        if (typeof arr2[key] === "undefined") {
            arr2[key] = [];
        }
        arr[key].push(parseInt(data[d].PVINPUTPOWER1));
        arr2[key].push(parseInt(data[d].PVINPUTPOWER2));
    }
    var pv1 = [];
    var pv2 = [];
    var labels = [];
    for (x in arr) {
        var resultGenerator = arr[x].reduce(function (sum, current) {
            return sum + current;
        }, 0);
        var resultGenerator2 = arr2[x].reduce(function (sum, current) {
            return sum + current;
        }, 0);


        labels.push(x);
        pv1.push((resultGenerator / arr[x].length).toFixed(1));
        pv2.push((resultGenerator2 / arr2[x].length).toFixed(1));
        resultGenerator = 0;

    }
    var pv = {
        labels: [],
        datasets: [],
        headerName: "[" + localize(invertorsList.location) + "]" + localize(invertor) + ": " + localize("massiveReport")
    };
    pv.labels = labels;
    pv.datasets.push({
        label: localize(invertor + 'PVINPUTPOWER1'),
        data: pv1,
        type: 'line',
        backgroundColor: "rgba(41, 125, 44, 0.5)"
    });
    pv.datasets.push({
        label: localize(invertor + 'PVINPUTPOWER2'),
        data: pv2,
        type: 'line',
        backgroundColor: "rgba(41, 0, 44, 0.5)"
    });
    vm2.graphicsdata.push(pv);
});

socket.on('HENERGY', function (data) {
    var henergy = {
        labels: [], datasets: [{label: localize('HENERGY'), data: [], type: 'line'}],
        headerName: "[" + localize(invertorsList.location) + "]" + localize(invertor) + ": " + localize("HENERGY")
    };
    var labels = [];
    var dataG = [];
    for (i in data) {
        labels.push(data[i].TRANDATE.replace("00:00:00.0", data[i].IHOUR));
        dataG.push(data[i].ENERGY);
    }
    //console.log(labels);
    //console.log(dataG);
    henergy.labels = labels;
    henergy.datasets[0].data = dataG;
    vm2.graphicsdata.push(henergy);
});

socket.on('listPVINPUTPOWER', function (data) {
    console.log(data);
});
socket.on('listPVINPUTVOLTAGE', function (data) {
    console.log(data);
});

socket.on('timeReport', function (data) {
    console.log(data);

    var timeReport = {
        labels: [], datasets: [{label: 'timeReport', data: []}],
        headerName: " "
    };
    var solarsFlag = false;
    var solarsStart = "";
    var solarsEnd = "";
    var arr = [];
    var key = "";
    var dailySolarArr = [];
    var dailyGeneratorArr = [];
    var generatorFlag = false;
    var generatorStart = "";
    var generatorEnd = "";
    for (i in data) {
        key = data[i].CURRENTTIME.split(" ")[0];
        if (typeof arr[key] === "undefined") {
            arr[key] = [];
        }
        arr[key].push({
            currenttime: data[i].CURRENTTIME,
            gridvoltager: data[i].GRIDVOLTAGER,
            totalacoutputactivepower: data[i].TOTALACOUTPUTACTIVEPOWER
        });
    }

    for (i in arr) {
        dailySolarArr[i] = [];
        dailyGeneratorArr[i] = [];
        for (x in arr[i]) {
            console.log("gridvoltager: " + arr[i][x].gridvoltager);
            if ((arr[i][x].gridvoltager != "0.0") && (x != (arr[i].length - 1))) {
                if (!generatorFlag) {
                    generatorFlag = true;
                    // console.log("start: ");
                    // console.log(arr[i][x].currenttime);
                    generatorStart = arr[i][x].currenttime;
                }
            }
            else
            // if((x == (arr[i].length - 1)) || (arr[i][x].gridvoltager == "0.0"))
            {
                if (generatorFlag) {
                    // console.log("end: ");
                    // console.log(arr[i][x].currenttime);
                    generatorFlag = false;
                    generatorEnd = arr[i][x].currenttime;
                    var start = new Date(generatorStart);
                    var end = new Date(generatorEnd);
                    //console.log("От генератора: start{" + generatorStart + "} end{" + generatorEnd + "} интервал: " + (end - start) / 60 / 60 / 1000);
                    dailyGeneratorArr[i].push({
                        start: solarsStart,
                        end: solarsEnd,
                        interval: ((end - start) / 60 / 60 / 1000)
                    });
                }
            }
            if ((x == (arr[i].length - 1)) || (arr[i][x].totalacoutputactivepower < 500)) {
                //ДЭС
                if (solarsFlag) {
                    solarsFlag = false;
                    solarsEnd = arr[i][x].currenttime;
                    var start = new Date(solarsStart);
                    var end = new Date(solarsEnd);
                    //console.log("От аккумуляторов: start{" + solarsStart + "} end{" + solarsEnd + "} интервал: " + (end - start) / 60 / 60 / 1000);
                    dailySolarArr[i].push({
                        start: solarsStart,
                        end: solarsEnd,
                        interval: ((end - start) / 60 / 60 / 1000)
                    });
                }
            }
            else if ((arr[i][x].totalacoutputactivepower >= 500)) {
                //АККУМУЛЯТОРЫ ИЛИ СОЛАРКА
                if (!solarsFlag) {
                    solarsFlag = true;
                    solarsStart = arr[i][x].currenttime;
                }

            }
        }
    }
    var timeGraph = {
        labels: [],
        datasets: [],
        headerName: "[" + localize(invertorsList.location) + "]" + localize(invertor) + ": " + localize("timeReport")
    };
    var labels = [];
    var generator = [];
    var solars = [];
    var daily = [];
    for (d in dailyGeneratorArr) {

        var slr = 0;
        var gnrtr = 0;
        //console.log(d + ": ");
        //console.log(dailyGeneratorArr[d]);
        for (z in dailyGeneratorArr[d]) {
            gnrtr += dailyGeneratorArr[d][z].interval;
        }
        for (z in dailySolarArr[d]) {
            slr += dailySolarArr[d][z].interval;
        }

        generator.push(Number(Number(gnrtr).toFixed(1)));
        solars.push(Number(Number(slr + (24 - gnrtr - slr)).toFixed(1)));
        labels.push(d.replace(/\d\d\d\d-/, ''));
        // labels.push(' 1 ');
        daily.push((24 - (slr + gnrtr)).toFixed(1));
        //console.log("slr: " + slr);
        //console.log("gnrtr: " + gnrtr);
    }


    timeGraph.labels = labels;

    var resultSolars = solars.reduce(function (sum, current) {
        return sum + current;
    }, 0);
    timeGraph.datasets.push({
        label: ('Работа от солн. станции, ч'),
        data: solars,
        fill: false,
        backgroundColor: "rgba(41, 125, 44, 0.5)"
    });
    var resultGenerator = generator.reduce(function (sum, current) {
        return sum + current;
    }, 0);
    timeGraph.datasets.push({
        label: ('работа от генератора, ч' ),
        data: generator,
        fill: false,
        backgroundColor: "rgba(135, 12, 41, 0.8)"
    });
    // timeGraph.datasets.push({label: ' ', data: daily});
    // , backgroundColor: "rgba(135, 116, 18, 0.8)"
    //console.log(timeGraph);
    var header = "[" + localize(invertorsList.location) + "][" + localize(invertor) + ": " + localize("timeReport")
            + "]"
            + '[Работа от солн. станции, ч. Итого: ' + resultSolars.toFixed(1)
            + '][работа от генератора, ч. Итого: ' + resultGenerator.toFixed(1)
        + ']'
        ;
    timeGraph.headerName = header;
    vm2.graphicsdata.push(timeGraph);
});

socket.on('DENERGY', function (data) {
    //console.log('denergy');

    var denergy = {
        labels: [],
        datasets: [{
            label: 'Выработка энергии за сутки, kwh',
            data: [],
            backgroundColor: "rgba(41, 125, 44, 0.5)",
            type: 'bar'
        }],
        headerName: "[" + localize(invertorsList.location) + "]" + localize(invertor) + ": " + localize("DENERGY")
    };
    var labels = [];
    var dataG = [];
    for (i in data) {
        //console.log(data[i].TRANDATE);
        //console.log(data[i].ENERGY);
        labels.push(data[i].TRANDATE.replace(" 00:00:00.0", ""));
        dataG.push(data[i].ENERGY);

    }

    //console.log(labels);
    //console.log(dataG);
    denergy.labels = labels;
    denergy.datasets[0].data = dataG;
    //console.log(denergy);
    vm2.graphicsdata.push(denergy);
});

socket.on('locationsList', function (data) {
    var locationsList = [];
    for (x in data) {
        var z = {text: localize(data[x]), id: data[x]}
        locationsList.push(z);
    }
    console.log(locationsList);
    locationList.elements = locationsList;
});

socket.on('invertorsLocation', function (location) {
    invertorsList.location = location;
    //console.log('list for ' + location);
});

socket.on('invertorsList', function (data) {
    console.log(data);
    optionsList.elements = [];
    var invertors = [];
    for (item in data) {
        invertors.push({id: data[item].SERIALNO, text: localize(data[item].SERIALNO)});
//            console.log(data[item].SERIALNO);
    }
    //console.log(invertors);
    invertorsList.elements = invertors;
});


// Vue.component('chart-item', {
//     props: ['id'],
//     template: '<canvas :id="id" width="400" height="400"></canvas>'
// });
//
// var charts = new Vue({
//     el: '#chartjs',
//     data: {
//         charts: [],
//         scripts: []
//     },
//     methods: {
//         newChart: function () {
//             this.charts.push({id: 'test'});
//             var ctx = document.getElementsByName("test");
//             // console.log(charts.charts[0]);
//             // ctx = charts.charts[0];
//             var myChart = new Chart(ctx, {
//                 type: 'bar',
//                 data: {
//                     labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
//                     datasets: [{
//                         label: '# of Votes',
//                         data: [12, 19, 3, 5, 2, 3],
//                         backgroundColor: [
//                             'rgba(255, 99, 132, 0.2)',
//                             'rgba(54, 162, 235, 0.2)',
//                             'rgba(255, 206, 86, 0.2)',
//                             'rgba(75, 192, 192, 0.2)',
//                             'rgba(153, 102, 255, 0.2)',
//                             'rgba(255, 159, 64, 0.2)'
//                         ],
//                         borderColor: [
//                             'rgba(255,99,132,1)',
//                             'rgba(54, 162, 235, 1)',
//                             'rgba(255, 206, 86, 1)',
//                             'rgba(75, 192, 192, 1)',
//                             'rgba(153, 102, 255, 1)',
//                             'rgba(255, 159, 64, 1)'
//                         ],
//                         borderWidth: 1
//                     }]
//                 },
//                 options: {
//                     scales: {
//                         yAxes: [{
//                             ticks: {
//                                 beginAtZero:true
//                             }
//                         }]
//                     }
//                 }
//             });
//         }
//     }
// });

Vue.component('element-item', {
    props: ['element', 'method', 'typeofdata'],
    template: '<li @click="callMethod">{{ element.text }}</li>',
    methods: {
        callMethod: function () {
            if (this.$parent[this.method])
            // console.log(this.element);
                this.$parent[this.method].call(this, this);
        }
    }
});


var reportsList = new Vue({
    el: '#reportsList',
    data: {
        elements: [{id: "time_report", text: localize("time_report")}, {
            id: "massivereport",
            text: localize("massivereport")
        }
        ],
        typeofdata: "getReport",
        methodname: "getData",
        invertor: "",
        seen: false
    },
    methods: {
        getData: function (e) {
            //console.log(e.element);
            var data = {
                label: e.typeofdata,
                element: e.element.id,
                invertor: optionsList.invertor,
                location: invertorsList.location,
                datestart: optionsList.datestart,
                dateend: optionsList.dateend
            };
            console.log();
            socket.emit(e.element.id, data);
        }
    }
});


var locationList = new Vue({
    el: '#locationList',
    data: {
        elements: [],
        typeofdata: "getInvertorsList",
        methodname: "getData"
    },
    methods: {
        getData: function (e) {
            console.log(e);
            var data = {label: e.typeofdata, element: e.element.id, location: invertorsList.location};
            reportsList.seen = false;
            invertorsList.location = e.element.id;
            invertorsList.locationtxt = e.element.text;
            socket.emit(e.typeofdata, data);
        }
    }
});

var invertorsList = new Vue({
    el: '#invertorsList',
    data: {
        location: "",
        locationtxt: "",
        elements: []
        , typeofdata: "getListAvailabelOptions",
        methodname: "getData"
    },
    methods: {
        getData: function (e) {
            //console.log(e);
            reportsList.seen = false;
            optionsList.elements = [];
            // console.log(e.element);
            // console.log(e.typeofdata);
            optionsList.invertor = e.element.id;
            invertor = e.element.id;
            //console.log(optionsList.invertor);
            console.log(invertor);
            var data = {label: e.typeofdata, element: e.element.id, location: invertorsList.location};
            socket.emit(e.typeofdata, data);

        }
    }
});

var optionsList = new Vue({
    el: '#optionsList',
    data: {
        elements: [],
        invertor: "",
        typeofdata: "getData",
        methodname: "getData",
        datestart: "",
        dateend: ""
    },
    methods: {
        getData: function (e) {
            if (e.element.id === "WORK_DATA") {
                reportsList.invertor = invertor;
                reportsList.seen = true;
            }
            else {

                var data = {
                    label: e.typeofdata,
                    element: e.element.id,
                    invertor: optionsList.invertor,
                    location: invertorsList.location,
                    datestart: this.datestart,
                    dateend: this.dateend
                };
                socket.emit(e.typeofdata, data);
            }
        }
    }
});


Vue.component('line-chart-new', {
    props: ['data', 'options', 'type'],
    extends: VueChartJs.Bar,
    mounted () {

        this.renderChart(this.data, {
            responsive: true,
            maintainAspectRatio: false,
            categoryPercentage: 1.0,
            testName: "sabaka"
            // barPercentage: 1.0,
            // barThickness: 50,
            // scales: {yAxes: [{stacked: true}]}
        });
    },
    methods: {
        callMethod: function () {
            if (this.$parent[this.method])
            //console.log(this.element);
                this.$parent[this.method].call(this, this);
        },
        remove: function (e) {

        }
    }

});

var vm2 = new Vue({
    el: '#graphics2',
    data: {
        message: 'Удалять графики левым кликом при зажатом CTRL',
        counter: 0,
        methodname: 'remove',
        graphicsdata: [
            // {
            //     // labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            //     //
            //     // datasets: [
            //     //     {
            //     //         label: 'Data One',
            //     //         backgroundColor: '#f87979',
            //     //
            //     //         data: [40, 39, 10, 40, 39, 80, 40]
            //     //     }
            //     // ]
            // },

        ]
    }
    ,
    methods: {
        remove: function (e) {
            // console.log(e);
            // console.log(e.target);
            console.log(e.target.parentNode.remove());
            // e.target.remove();
        }
    }
});

